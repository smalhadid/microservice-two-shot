# Wardrobify

Team:

* Shahem Al Hadid - Shoes
* Ryan Curry - Hats

## Design
We used domain driven design to build microservices. We have a wardrobe microservice, a shoe microservice and a hats microservice that allow us to manage our inventory. We used REST APIs for each microservice. We integrated the React framework to build a frontend UI to allow users to interact(create, read, delete) with our application and manage their inventory. We used docker containers to manage each service.
## Shoes microservice

Created 2 models, a shoe model and a bin value object. The shoe model represents a shoe stored in a bin thats in our wardrobe microservice. The bin value object links our shoes to the bin in our wardrobe microservice. Whenever a shoe is assigned to a bin, a bin value object is created in our shoe microservice database as a column in the table to link the bin to the shoe.

## Hats microservice

Created 2 models, a hat model and a location value object. The hat model represents a hat stored in a location thats in our wardrobe microservice. The location value object links our hats to the location in our wardrobe microservice. Whenever a hat is assigned to a location, a hat value object is created in our hat microservice database as a column in the table to link the location to the hat.
