from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Hat, LocationVO


class HatEncoder(ModelEncoder):
    model = Hat
    properties = ["id", "fabric", "style_name", "color", "picture_url", "location"]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}


# Create your views here.
@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    # function name might not be descriptive enough
    """
    Collection RESTful API handler for Hat objects in
    the wardrobe.

    GET:
    Returns a dictionary with a single key "hats" which
    is a list of the fabric, style name, color, picture URL, along with its href and id.

    {
        "hats": [
            {
                "fabric": hat fabric,
                "style_name": style of the hat,
                "color": hat color,
                "picture_url": picture URL,
                "href": URL to the location,
            },
            ...
        ]
    }

    POST:
    Creates a hat resource and returns its details.
    {
        "fabric": hat fabric,
        "style_name": style of the hat,
        "color": hat color,
        "picture_url": picture URL,
    }
    """
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatEncoder,
        )
    else:
        content = json.loads(request.body)
        # get Location object and put in the content dict
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_hat(request, pk):
    # do not like this function name
    """
    Returns the details for the Hat model specified
    by the pk parameter.

    This should return a dictionary with the id, fabric, style_name,
    color, and picture_url.

    {
        "hat": {
                "id": primary key (pk),
                "fabric": hat fabric,
                "style_name": style of the hat,
                "color": hat color,
                "picture_url": picture URL
            }
    }
    """
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=pk)
            return JsonResponse(
                {"hat": hat},
                encoder=HatEncoder,
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": f"Hat {pk} does not exist"})
    elif request.method == "DELETE":
        try:
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": f"Hat {pk} does not exist"})
    else:
        content = json.loads(request.body)
        try:
            Hat.objects.filter(id=pk).update(**content)
            hat = Hat.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": f"Hat {pk} does not exist"})
