from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()

    def __str__(self):
        return f"{self.closet_name} - {self.section_number}/{self.shelf_number}"


class Hat(models.Model):
    """
    The Hat model represents a hat within the wardrobe.
    """

    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)

    location = models.ForeignKey(
        LocationVO, related_name="hats", on_delete=models.CASCADE, null=True
    )

    def get_api_url(self):
        return reverse("api_hat", kwargs={"pk": self.pk})
