import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HatList';
import HatForm from './HatForm';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route path="/hats" element={<HatList></HatList>} />
            <Route path="new" element={<HatForm />} />
          </Route>
          <Route path="shoes">
            <Route path="/shoes" element={<ShoeList></ShoeList>} />
            <Route path="new" element={<ShoeForm />} />
          </Route>
        </Routes>
    </BrowserRouter>
  );
}

export default App;
