import {useState, useEffect} from 'react'; 

function HatList() {

  const [hats, setHats] = useState([]);

  async function loadHats() {
    const response = await fetch('http://localhost:8090/api/hats/');
    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    } else {
      console.error(response);
    }
  };

    const handleDelete = async (id) => {

        const url = `http://localhost:8090/api/hats/${id}/`

        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json'
            },
        }

        const response = await fetch(url, fetchConfig);
        console.log(response);
        if (response.ok) {
            window.location.reload(false);
        }
    }

    useEffect(() => {
      loadHats();
    }, []);
    
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Fabric</th>
            <th>Style Name</th>
            <th>Color</th>
            <th>Picture URL</th>
            <th>Location</th>
          </tr>
        </thead>
        <tbody>
          {hats.map(hat => {
            return (
              <tr key={hat.id}>
                <td>{ hat.fabric }</td>
                <td>{ hat.style_name }</td>
                <td>{ hat.color }</td>
                <td>{ hat.picture_url }</td>
                <td>{ hat.location }</td>
                <td><button onClick={() => handleDelete(hat.id)}>Delete</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
  
  export default HatList;