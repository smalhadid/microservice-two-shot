import {useState, useEffect} from 'react';

function ShoeList() {

    const [shoes, setShoes] = useState([]);

    async function loadShoes() {

        const response = await fetch('http://localhost:8080/api/shoes/');

        if (response.ok) {
          const data = await response.json();
          setShoes(data.shoes);
        } else {
          console.error(response);
        }
      };


    const handleDelete = async (id) => {
        const deleteUrl = `http://localhost:8080/api/shoes/${id}/`

        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json',
            }
        }
        console.log(fetchConfig)
        const response = await fetch(deleteUrl, fetchConfig)
        console.log(response)
        if (response.ok) {
            window.location.reload(false)
        }
    }

        useEffect(() => {
        loadShoes();
        }, []);


    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>model_name</th>
                    <th>color</th>
                    <th>picture_url</th>
                    <th>bin</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map(shoe => {
                    return (
                        <tr key={shoe.id}>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.model_name}</td>
                            <td>{shoe.color}</td>
                            <td>{shoe.picture_url}</td>
                            <td>{shoe.bin}</td>
                            <td><button onClick={() => handleDelete(shoe.id)}>Delete</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default ShoeList;
